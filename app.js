require("dotenv").config();
require("colors");
const express = require("express");
const morgan = require("morgan");
const path = require("path");
const layout = require("express-ejs-layouts");
const session = require("express-session");
const methodOverride = require("method-override");

const app = express();
const PORT = process.env.PORT || 3000;

// middlewares
app.use(session({
  secret: process.env.SECRET_SESSION,
  resave: false,
  saveUninitialized: false
}))
app.use(methodOverride('_method'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(morgan("dev"));
app.use("/", express.static(path.join(__dirname, "public")));

// set engine view
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(layout);

// routes web
// $2b$10$h4fYr79VBauLfNiQBMYpHO/JYRLU/oFHBtxonLzV/yTxXGgWCojB2 admin123
app.use("/", require("./src/routes/homeRoute"));
app.use("/users", require("./src/routes/userRoute"));

// routes api
app.use("/api/v1/games", require("./src/routes/gameRoute"));
app.use("/api/v1/users", require("./src/routes/userAPIRoute"));

// not found route
app.use(require("./src/utils/response").notFoundHandler);

// erorr handler
app.use(require("./src/utils/response").errorHandler);

app.listen(PORT, () => {
  console.log(`Server listening on http://${process.env.HOST}:${PORT}`.blue);
});
