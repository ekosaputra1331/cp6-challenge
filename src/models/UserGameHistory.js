const { Model, DataTypes } = require("sequelize");
const sequelize = require("../config/db");

class UserGameHistory extends Model {}

UserGameHistory.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      // reff
    },
    game_id: {
      type: DataTypes.INTEGER,
      // reff
    },
    score: {
      type: DataTypes.INTEGER,
    },
    played_at: {
      type: DataTypes.DATE,
    },
  },
  {
    tableName: "user_game_histories",
    modelName: "UserGameHistory",
    sequelize,
    timestamps: false,
  }
);

module.exports = UserGameHistory;
