const { DataTypes, Model } = require("sequelize");
const sequelize = require("../config/db");

class Game extends Model {}

Game.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    description: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    tableName: "games",
    modelName: "Game",
    timestamps: false,
  }
);

module.exports = Game;
