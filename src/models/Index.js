const UserBio = require("./UserBio");
const Game = require("./Game");
const User = require("./User");
const UserGameHistory = require("./UserGameHistory");

User.hasOne(UserBio, {
  sourceKey: "id",
  foreignKey: "user_id",
  as: "bio",
});

UserBio.belongsTo(User, {
  sourceKey: "user_id",
  foreignKey: "id",
  as: "user",
});

User.belongsToMany(Game, {
  through: UserGameHistory,
  foreignKey: "user_id",
});

Game.belongsToMany(User, {
  through: UserGameHistory,
  foreignKey: "game_id",
});

UserGameHistory.belongsTo(User, {
  foreignKey: "user_id",
  as: "user",
});

UserGameHistory.belongsTo(Game, {
  foreignKey: "game_id",
  as: "game",
});

User.hasMany(UserGameHistory, {
  foreignKey: "user_id",
  as: "game_histories",
});

Game.hasMany(UserGameHistory, {
  foreignKey: "game_id",
  as: "user_histories",
});

module.exports = {
  UserBio,
  User,
  UserGameHistory,
  Game,
};
