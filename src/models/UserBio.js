const { Model, DataTypes } = require("sequelize");
const sequelize = require("../config/db");
class UserBio extends Model {}

UserBio.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "User",
        key: "id",
      },
    },
    first_name: {
      type: DataTypes.STRING,
    },
    last_name: {
      type: DataTypes.STRING,
    },
    hobby: {
      type: DataTypes.STRING,
    },
    address: {
      type: DataTypes.TEXT,
    },
  },
  {
    tableName: "user_bios",
    modelName: "UserBio",
    sequelize,
    timestamps: false,
  }
);

module.exports = UserBio;
