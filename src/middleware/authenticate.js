function isAuthenticated(req, res, next) {
  if(req.session.user) return next();

  res.redirect('/login');
}

function isLoggedIn(req, res, next) {
  if(!req.session.user) return next();

  res.redirect('/');
}

module.exports = {
  isAuthenticated,
  isLoggedIn
}