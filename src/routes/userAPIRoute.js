const router = require("express").Router();
const UserAPIController = require("../controllers/UserAPIController");

router.get("/", UserAPIController.index);
router.get("/:id", UserAPIController.show);
router.post("/", UserAPIController.create);
router.put("/:id", UserAPIController.update);
router.delete("/:id", UserAPIController.destroy);

module.exports = router;
