const router = require("express").Router();
const UserController = require("../controllers/UserController");
const { isAuthenticated } = require("../middleware/authenticate");

router.get("/", isAuthenticated, UserController.index);
router.get("/:id", isAuthenticated, UserController.show);
router.post("/", isAuthenticated, UserController.create);
router.put("/:id", isAuthenticated, UserController.update);
router.delete("/:id", isAuthenticated, UserController.destory);
router.get("/:id/history", isAuthenticated, UserController.showHistory);
router.post("/userhistory", isAuthenticated, UserController.userHistory);

module.exports = router;
