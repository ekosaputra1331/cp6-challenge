const router = require("express").Router();
const GameController = require("../controllers/GameController");

router.get("/", GameController.index);
router.get("/:id", GameController.show);
router.post("/", GameController.create);
router.put("/:id", GameController.update);
router.delete("/:id", GameController.destroy);

module.exports = router;
