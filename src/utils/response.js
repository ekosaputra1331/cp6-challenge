const ValidationError = require("../utils/ValidateError")

function success(res, message, data, code = 200){
  res.status(code).json({
    success: true,
    result: data,
    message,
    code
  })
}

function error(res, err, code = 500){
  if(err instanceof ValidationError){
    return res.status(err.code).json({
      success: false,
      message: err.message,
      code: err.code
    })
  }

  res.status(code).json({
    success: false,
    message: err.message || err,
    code: err.code || code,
  })
}

function notFoundHandler(req, res){
  return res.status(404).json({
    success: false,
    message: 'Not Found',
    code: 404
  })
}

function errorHandler(err, req, res, next) {
  return error(res, err);
}

module.exports = {
  success,
  error,
  notFoundHandler,
  errorHandler
}