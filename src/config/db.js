const { Sequelize } = require("sequelize");

const sequelize = new Sequelize(
  `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, {
    dialect: 'postgres',
  }
);

(async () => {
  try {
    await sequelize.authenticate();
    console.log(`DB authentication successful`.blue);  
  } catch (error) {
    console.log(`Error : ${error.message}`);
    process.exit(1);
  }
})();

module.exports = sequelize;
