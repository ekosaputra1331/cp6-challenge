$('.delete-form').on('submit',function(e) {
  e.preventDefault();
  Swal.fire({
    title: 'Are you sure to delete this user?',
    showCancelButton: true,
    confirmButtonText: 'Delete',
    confirmButtonColor: '#dc3545',
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      Swal.fire({
        title: 'Loading...',
        showConfirmButton: false,
      })

      $(this).unbind('submit').submit();
    } 
  })
})

$('#logout-button').on('submit',function(e) {
  e.preventDefault();
  Swal.fire({
    title: 'Are you sure to logout?',
    showCancelButton: true,
    confirmButtonText: 'Logout',
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      Swal.fire({
        title: 'Loading...',
        showConfirmButton: false,
      })

      $(this).unbind('submit').submit();
    } 
  })
})
